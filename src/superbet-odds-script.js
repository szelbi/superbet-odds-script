// GLOBAL VARIABLES
const delay = (ms) => new Promise((res) => setTimeout(res, ms));

const OutputModes = Object.freeze({
  odds: 1,
  multipliers: 2,
});

const originalHref = window.location.href;
const eventsLength = document.querySelectorAll("div.event-card").length;
const originalResultSeparator = ":";

let i = 0;
let matchesOdds = [];
// GLOBAL VARIABLES end
// ====================

// CONFIGURATION
// Change this value to adjust the separator used in the match results output
const targetResultSeparator = "-";

// Change this value to toggle between odds and multipliers output
const outputMode = OutputModes.multipliers;

// Adjust the following array to change the multipliers and their ranges
const multipliers = [
  {
    low: 0,
    high: 14,
    multiplier: 1,
  },
  {
    low: 14,
    high: 50,
    multiplier: 2,
  },
  {
    low: 50,
    high: Infinity,
    multiplier: 3,
  },
];

// Change this value to adjust the timeout between opening match pages, before reading odds, etc.
const timeout = 500;
// CONFIGURATION end
// =================

// FUNCTIONS
function getMultiplier(number) {
  if (number < 1.0) {
    throw new Error("Number must be greater than or equal to 1.0");
  }

  for (const multiplier of multipliers) {
    if (number >= multiplier.low && number < multiplier.high) {
      return multiplier.multiplier;
    }
  }

  // Default for infinity
  return multipliers[multipliers.length - 1].multiplier;
}

function matchesOddsToMultipliers(matchesOdds) {
  let matchesMultipliers = [];
  for (const match of matchesOdds) {
    const multipliers = new Map();

    for (const [key, value] of match.values.entries()) {
      multipliers.set(key, getMultiplier(value));
    }

    matchesMultipliers.push({
      name: match.name,
      values: multipliers,
    });
  }
  return matchesMultipliers;
}

function valuesToColumns(matchesValues) {
  const columnNames = new Set(["match"]);

  for (const match of matchesValues) {
    for (const valuesKey of match.values.keys()) {
      columnNames.add(valuesKey);
    }
  }

  let outputColumns = [];

  let firstColumn = true;
  for (const columnName of columnNames) {
    let column = [];
    column.push(columnName);

    for (const match of matchesValues) {
      if (firstColumn) {
        column.push(match.name);
        continue;
      }

      const value = match.values.get(columnName);
      if (value === undefined) {
        column.push("no value");
        continue;
      }
      column.push(value);
    }

    outputColumns.push(column);

    if (firstColumn) {
      firstColumn = false;
    }
  }

  return outputColumns;
}

function columnsToCsv(columns) {
  let outputRows = [];

  for (const column of columns) {
    for (const [rowIndex, rowValue] of column.entries()) {
      if (outputRows[rowIndex] === undefined) {
        outputRows[rowIndex] = "";
      }
      outputRows[rowIndex] = `${outputRows[rowIndex]}${rowValue};`;
    }
  }

  return outputRows.join("\n");
}

async function openMatchPages() {
  const currentUrl = window.location.href;

  if (document.readyState === "complete") {
    if (currentUrl !== originalHref) {
      try {
        const matchOdds = await readOdds();
        matchesOdds.push(matchOdds);
      } catch (error) {
        console.error(error.message);
      }

      window.history.back();
    }

    if (i < eventsLength) {
      setTimeout(openMatchPages, timeout);
    } else {
      try {
        const columns = valuesToColumns(
          outputMode === OutputModes.odds
            ? matchesOdds
            : matchesOddsToMultipliers(matchesOdds)
        );
        console.log(columnsToCsv(columns));
      } catch (error) {
        console.error(error.message);
      }
    }

    const events = document.querySelectorAll("div.event-card");
    if (events.length === 0) {
      return;
    }

    events[i].click();
    i++;
  }
}

function getMatchName() {
  const teamsDiv = document.querySelector("div.teams-container");
  if (!teamsDiv) {
    return;
  }

  const teamsNameSpans = Array.from(
    teamsDiv.querySelectorAll("span.team-name")
  );
  if (teamsNameSpans.length === 0) {
    return;
  }

  const teamNames = teamsNameSpans.map((element) => element.textContent.trim());
  const matchName = teamNames.join(" - ");

  return matchName;
}

async function readOdds() {
  // Wait for the page to load JS
  await delay(timeout);

  const currentUrl = window.location.href;

  const exactResultDiv = Array.from(
    document.body.querySelectorAll("div.single-market-card__header")
  ).find((element) => element.textContent.trim() === "Dokładny wynik");
  if (!exactResultDiv) {
    throw new Error(
      `Failed to find exact results section for the following match: ${currentUrl}`
    );
  }

  exactResultDiv.click();

  // Wait for the section to expand
  await delay(timeout);

  const exactResultButtons = Array.from(
    document.querySelectorAll(`button[title="Dokładny wynik"]`)
  );

  if (exactResultButtons.length === 0) {
    throw new Error(
      `Failed to find exact result buttons for the following match: ${currentUrl}`
    );
  }

  const odds = new Map();

  for (const exactResultButton of exactResultButtons) {
    const oddNameSpan = exactResultButton.querySelector(".e2e-odd-name");
    if (!oddNameSpan) {
      throw new Error(
        `Failed to find odd name span for the following button: ${exactResultButton} (${currentUrl})`
      );
    }

    const oddCurrentValueSpan = exactResultButton.querySelector(
      ".e2e-odd-current-value"
    );
    if (!oddCurrentValueSpan) {
      throw new Error(
        `Failed to find odd current value span for the following button: ${exactResultButton} (${currentUrl})`
      );
    }

    // Replace colons with hyphens in results
    let oddName = oddNameSpan.textContent.trim();
    if (originalResultSeparator !== targetResultSeparator) {
      oddName = oddName.replace(originalResultSeparator, targetResultSeparator);
    }

    const oddCurrentValue = oddCurrentValueSpan.textContent.trim();

    odds.set(oddName, oddCurrentValue);
  }

  return {
    name: getMatchName(),
    values: odds,
  };
}
// FUNCTIONS end
// =============

openMatchPages();
