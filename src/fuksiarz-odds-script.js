// GLOBAL VARIABLES
const delay = (ms) => new Promise((res) => setTimeout(res, ms));

const OutputModes = Object.freeze({
  odds: 1,
  multipliers: 2,
});

const originalHref = window.location.href;
const buttonsLength = document.querySelectorAll(
  "div.after-event > button"
).length;
const originalResultSeparator = ":";

let matchPagesCounter = 0;
let matchesOdds = [];
// GLOBAL VARIABLES end
// ====================

// CONFIGURATION
// Change this value to adjust the separator used in the match results output
const targetResultSeparator = "-";

// Change this value to toggle between odds and multipliers output
const outputMode = OutputModes.multipliers;

// Adjust the following array to change the multipliers and their ranges
const multipliers = [
  {
    low: 0,
    high: 14,
    multiplier: 1,
  },
  {
    low: 14,
    high: 50,
    multiplier: 2,
  },
  {
    low: 50,
    high: Infinity,
    multiplier: 3,
  },
];

// Change this value to adjust the timeout between opening match pages, before reading odds, etc.
const timeout = 500;
// CONFIGURATION end
// =================

// FUNCTIONS
function getMultiplier(number) {
  if (number < 1.0) {
    throw new Error("Number must be greater than or equal to 1.0");
  }

  for (const multiplier of multipliers) {
    if (number >= multiplier.low && number < multiplier.high) {
      return multiplier.multiplier;
    }
  }

  // Default for infinity
  return multipliers[multipliers.length - 1].multiplier;
}

function matchesOddsToMultipliers(matchesOdds) {
  let matchesMultipliers = [];
  for (const match of matchesOdds) {
    const multipliers = new Map();

    for (const [key, value] of match.values.entries()) {
      multipliers.set(key, getMultiplier(value));
    }

    matchesMultipliers.push({
      name: match.name,
      values: multipliers,
    });
  }
  return matchesMultipliers;
}

function valuesToColumns(matchesValues) {
  const columnNames = new Set(["match"]);

  for (const match of matchesValues) {
    for (const valuesKey of match.values.keys()) {
      columnNames.add(valuesKey);
    }
  }

  let outputColumns = [];

  let firstColumn = true;
  for (const columnName of columnNames) {
    let column = [];
    column.push(columnName);

    for (const match of matchesValues) {
      if (firstColumn) {
        column.push(match.name);
        continue;
      }

      const value = match.values.get(columnName);
      if (value === undefined) {
        column.push("no value");
        continue;
      }
      column.push(value);
    }

    outputColumns.push(column);

    if (firstColumn) {
      firstColumn = false;
    }
  }

  return outputColumns;
}

function columnsToCsv(columns) {
  let outputRows = [];

  for (const column of columns) {
    for (const [rowIndex, rowValue] of column.entries()) {
      if (outputRows[rowIndex] === undefined) {
        outputRows[rowIndex] = "";
      }
      outputRows[rowIndex] = `${outputRows[rowIndex]}${rowValue};`;
    }
  }

  return outputRows.join("\n");
}

async function openMatchPages() {
  const currentUrl = window.location.href;

  if (document.readyState === "complete") {
    if (currentUrl !== originalHref) {
      try {
        const matchOdds = await readOdds();
        matchesOdds.push(matchOdds);
      } catch (error) {
        console.error(error.message);
      }

      window.history.back();
    }

    if (matchPagesCounter < buttonsLength) {
      setTimeout(openMatchPages, timeout);
    } else {
      try {
        const columns = valuesToColumns(
          outputMode === OutputModes.odds
            ? matchesOdds
            : matchesOddsToMultipliers(matchesOdds)
        );
        console.log(columnsToCsv(columns));
      } catch (error) {
        console.error(error.message);
      }
    }

    const buttons = document.querySelectorAll("div.after-event > button");
    if (buttons.length === 0) {
      return;
    }

    buttons[matchPagesCounter].click();
    matchPagesCounter++;
  }
}

function getMatchName() {
  const eventBoxDiv = document.querySelector("div.event-box");
  if (!eventBoxDiv) {
    return;
  }

  const teamsNameSpans = Array.from(eventBoxDiv.querySelectorAll("li > span"));
  if (teamsNameSpans.length === 0) {
    return;
  }

  const teamNames = teamsNameSpans.map((element) => element.textContent.trim());
  const matchName = teamNames.join(" - ");

  return matchName;
}

async function readOdds() {
  // Wait for the page to load JS
  await delay(timeout);

  const currentUrl = window.location.href;

  const exactResultHeaderDiv = Array.from(
    document.body.querySelectorAll("div.marketTypesListItemHeader")
  ).find((element) => element.textContent.trim() === "Dokładny wynik");
  if (!exactResultHeaderDiv) {
    throw new Error(
      `Failed to find exact results section for the following match: ${currentUrl}`
    );
  }

  if (!exactResultHeaderDiv.classList.contains("is-expanded")) {
    const exactResultSectionExpander = exactResultHeaderDiv.querySelector("i");
    exactResultSectionExpander.click();
    // Wait for the section to expand
    await delay(timeout);
  }

  const exactResultSectionDiv = exactResultHeaderDiv.parentNode;
  const exactResultVariantNameDiv = Array.from(
    exactResultSectionDiv.querySelectorAll("div.game-name")
  ).find((element) => element.textContent.trim() === "Dokładny wynik");
  if (!exactResultVariantNameDiv) {
    throw new Error(
      `Failed to find exact result variant for the following match: ${currentUrl}`
    );
  }

  const exactResultVariantDiv = exactResultVariantNameDiv.parentNode;
  const exactResultVariantExpander = exactResultVariantDiv.querySelector("i");
  const exactResultVariantExpandButtonDiv =
    exactResultVariantExpander.parentNode;
  if (!exactResultVariantExpandButtonDiv.classList.contains("expanded")) {
    exactResultVariantExpander.click();
    // Wait for the section to expand
    await delay(timeout);
  }

  const exactResultButtonWrappers = Array.from(
    exactResultVariantDiv.querySelectorAll(
      `div.outcome-button-wrapper.correct-score`
    )
  );
  if (exactResultButtonWrappers.length === 0) {
    throw new Error(
      `Failed to find exact result button wrappers for the following match: ${currentUrl}`
    );
  }

  const odds = new Map();

  for (const exactResultButtonWrapper of exactResultButtonWrappers) {
    const oddNameSpan =
      exactResultButtonWrapper.querySelector("span.outcome-name");
    if (!oddNameSpan) {
      throw new Error(
        `Failed to find odd name span for the following button wrapper: ${exactResultButtonWrapper} (${currentUrl})`
      );
    }

    const oddValueSpan =
      exactResultButtonWrapper.querySelector("span.outcome-odd");
    if (!oddValueSpan) {
      throw new Error(
        `Failed to find odd value span for the following button wrapper: ${exactResultButtonWrapper} (${currentUrl})`
      );
    }

    // Replace colons with hyphens in results
    let oddName = oddNameSpan.textContent.trim();
    if (originalResultSeparator !== targetResultSeparator) {
      oddName = oddName.replace(originalResultSeparator, targetResultSeparator);
    }
    const oddCurrentValue = oddValueSpan.textContent.trim();

    odds.set(oddName, oddCurrentValue);
  }

  return {
    name: getMatchName(),
    values: odds,
  };
}
// FUNCTIONS end
// =============

openMatchPages();
